﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using subtoole.Models;

namespace subtoole.Models
{
    partial class Person
    {
        public string FullName => $"{FirstName} {LastName}";
    }
}

namespace subtoole.Controllers
{
    public class PeopleController : Controller
    {
        private Entities db = new Entities();

        // GET: People
        public ActionResult Index()
        {
            if (User.IsInRole("Administraator"))
                return View(db.People.ToList());
            else return RedirectToAction("Index", "Home");
        }

        // GET: People/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }



            return View(person);
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult AddPartner(string id, string partner)
        {
            Company c = db.Companies.Find(id);
            Person p = db.People.Find(partner);
            if (c != null && p != null)
            {
                try
                {
                    c.People.Add(p);
                    db.SaveChanges();
                }
                catch { }
            }
            return RedirectToAction("Details", new { id });
        }

        public ActionResult RemovePartner(string id, string partner)
        {
            Company c = db.Companies.Find(id);
            Person p = db.People.Find(partner);
            if (c != null && p != null)
            {
                try
                {
                    c.People.Remove(p);
                    db.SaveChanges();
                }
                catch { }
            }
            return RedirectToAction("Details", new { id });
        }

        // GET: People/Create
        public ActionResult Create()
        {
            ViewBag.PictureId = new SelectList(db.Files, "Id", "Filename");

            return View();
        }

        // POST: People/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,FirstName,LastName,Email,BirtDate,Gender,IK,Address,Phone,Education,Profession,Languages,CV,PictureId")] Person person)
        {
            if (ModelState.IsValid)
            {
                db.People.Add(person);
                db.SaveChanges();
                return RedirectToAction("Create", "Applications", new { id = person.Id});
                
            }

            ViewBag.PictureId = new SelectList(db.Files, "Id", "Filename", person.PictureId);
            return View(person);
        }

        // GET: People/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            ViewBag.PictureId = new SelectList(db.Files, "Id", "Filename", person.PictureId);
            return View(person);
        }

        // POST: People/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,FirstName,LastName,Email,BirtDate,Gender,IK,Address,Phone,Education,Profession,Languages,CV,PictureId")] Person person)
        {
            if (ModelState.IsValid)
            {
                db.Entry(person).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PictureId = new SelectList(db.Files, "Id", "Filename", person.PictureId);
            return View(person);
        }

        // GET: People/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // POST: People/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Person person = db.People.Find(id);
            db.People.Remove(person);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
