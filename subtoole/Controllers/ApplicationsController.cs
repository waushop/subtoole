﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using subtoole.Models;


namespace subtoole.Controllers
{
    public class ApplicationsController : Controller
    {
        private Entities db = new Entities();
        
        // GET: Applications
        public ActionResult Index()
        {
            
            //TODO: vahele veel tingimuse
            // kui inimene on kontaktisik, siis näidatakse talle taotlusi, mis on tema esindatud restoranidesse
            // kui inimene EI OLE kontaktisik (person.Companies.count == 0), siis näidatakse talle TEMA taotlusi

            Person person = db.People.Where(x => x.Email == User.Identity.Name).SingleOrDefault();
            if (person == null) // kasutaja pole sisse loginud või on meile võõras, mis sa teed
            {
                return RedirectToAction("Index", "Home");
            }
            var comps = person.Companies.Select(c => c.Id).ToList();
            if (comps.Count == 0) // kasutaja ei ole restorani esindaja
            {
                return View(person.Applications.ToList());
            }
            else // kasutaja on vähemalt ühe restorani esindaja
            {
                return View( db.Applications.Where(x => comps.Contains(x.CompanyId)).ToList());
            }

            
        }

        // GET: Applications/Details/5
        [Authorize]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Application application = db.Applications.Find(id);
            if (application == null)
            {
                return HttpNotFound();
            }
            return View(application);
        }
        
        // GET: Applications/Create
        public ActionResult Create(int id)
        {
            Person person = db.People.Find(id);
            if (person == null) return HttpNotFound();
            //ViewBag.PersonId = new SelectList(db.People, "Id", "FirstName");
            ViewBag.CompanyId = new SelectList(db.Companies, "Id", "Name");
            return View(new Application { PersonId = id, ValidFrom = DateTime.Today, ValidThru = DateTime.Today.AddDays(30), Created = DateTime.Today  });
            
        }
        
        // POST: Applications/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,PersonId,CompanyId,Created,ValidFrom,ValidThru,PrefTime,State,Load")] Application application)
        {
            if (ModelState.IsValid)
            {
                db.Applications.Add(application);
                db.SaveChanges();

                Session["email"] = db.People.Find(application.PersonId).Email;
                return View("Success");
            }

            ViewBag.PersonId = new SelectList(db.People, "Id", "FirstName", application.PersonId);
            ViewBag.CompanyId = new SelectList(db.Companies, "Id", "Name", application.CompanyId);
            
            return View("Success");
        }
        // GET: Applications/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Application application = db.Applications.Find(id);
            if (application == null)
            {
                return HttpNotFound();
            }
            ViewBag.PersonId = new SelectList(db.People, "Id", "FirstName", application.PersonId);
            ViewBag.CompanyId = new SelectList(db.Companies, "Id", "Name", application.CompanyId);
            return View(application);
        }

        // POST: Applications/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,PersonId,CompanyId,Created,ValidFrom,ValidThru,PrefTime,State,Load")] Application application)
        {
            if (ModelState.IsValid)
            {
                db.Entry(application).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PersonId = new SelectList(db.People, "Id", "FirstName", application.PersonId);
            ViewBag.CompanyId = new SelectList(db.Companies, "Id", "Name", application.CompanyId);
            return View(application);
        }

        // GET: Applications/Delete/5
        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Application application = db.Applications.Find(id);
            if (application == null)
            {
                return HttpNotFound();
            }
            return View(application);
        }

        // POST: Applications/Delete/5
        [Authorize]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Application application = db.Applications.Find(id);
            db.Applications.Remove(application);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        
    }
}
