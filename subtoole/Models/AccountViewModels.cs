﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace subtoole.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "E-Post")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Kood")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Jäta mind meelde?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "E-Post")]
        public string Email { get; set; }
       
    }

public class LoginViewModel
    {
        [Required]
        [Display(Name = "E-Post")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Salasõna")]
        public string Password { get; set; }

        [Display(Name = "Jäta mind meelde?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required]
        [Display(Name = "Kasutajatunnus")]
        public string DisplayName { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "E-Post")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Salasõna")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Kinnita salasõna")]
        [Compare("Password", ErrorMessage = "Salasõnad ei kattu!")]
        public string ConfirmPassword { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "E-Post")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Salasõna")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Kinnita salasõna")]
        [Compare("Password", ErrorMessage = "Salasõnad ei kattu!")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "E-Post")]
        public string Email { get; set; }
    }
}
