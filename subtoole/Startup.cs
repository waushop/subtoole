﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(subtoole.Startup))]
namespace subtoole
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
